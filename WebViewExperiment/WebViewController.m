//
//  WebViewController.m
//  WebViewExperiment
//
//  Created by Melanie Lislie Hsu on 8/12/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () {
    NSURL *URL;
    WKWebView *wkView;
}

@end

@implementation WebViewController

- (id)initWithURL:(NSURL *)url {
    self = [super init];
    if (self) {
        URL = url;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
    //Clear All Cookies
    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        //if([[cookie domain] isEqualToString:someNSStringUrlDomain]) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    UIImage *backImage = [UIImage imageNamed:@"BackButton.imageset"];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStylePlain target:self action:@selector(onBackButtonPress:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    wkView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) configuration:theConfiguration];
    wkView.navigationDelegate = self;
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:URL];
    [wkView loadRequest:nsrequest];
    [self.view addSubview:wkView];
}

- (IBAction)onBackButtonPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"Web view deallocated.");
    [wkView loadHTMLString:@"" baseURL:nil];
    [wkView stopLoading];
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
