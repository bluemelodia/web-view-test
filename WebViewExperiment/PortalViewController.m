//
//  PortalViewController.m
//  WebViewExperiment
//
//  Created by Melanie Lislie Hsu on 8/12/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "PortalViewController.h"
#import "WebViewController.h"

@interface PortalViewController ()

@end

@implementation PortalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Touchdown");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onMadScientistPress:(id)sender {
    NSURL *madURL = [[NSURL alloc] initWithString:@"http://www.thinkgeek.com/"];
    WebViewController *madScientistWeb = [[WebViewController alloc] initWithURL:madURL];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:madScientistWeb];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)onHazardousChemicalsPress:(id)sender {
    NSURL *hazardURL = [[NSURL alloc] initWithString:@"http://www.firemountaingems.com/"];
    WebViewController *hazardousChemicalsWeb = [[WebViewController alloc] initWithURL:hazardURL];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:hazardousChemicalsWeb];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)onZombiesPress:(id)sender {
    NSURL *zombieURL = [[NSURL alloc] initWithString:@"http://www.thezombiehunters.com/index.php"];
    WebViewController *zombieWeb = [[WebViewController alloc] initWithURL:zombieURL];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:zombieWeb];
    [self presentViewController:navController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
