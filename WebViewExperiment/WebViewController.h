//
//  WebViewController.h
//  WebViewExperiment
//
//  Created by Melanie Lislie Hsu on 8/12/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
@import WebKit;

@interface WebViewController : UIViewController <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (id)initWithURL:(NSURL *)url;

@end
