//
//  AppDelegate.h
//  WebViewExperiment
//
//  Created by Melanie Lislie Hsu on 8/12/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

